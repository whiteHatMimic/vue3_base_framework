import { createApp } from 'vue';
import router from './router';
import store from './store';

// js
import './permission.js';

// css
import 'normalize.css';
import '@/styles/index.sass';

// components
import App from './App.vue';

createApp(App)
    .use(store)
    .use(router)
    .mount('#app');
