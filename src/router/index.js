import { createRouter, createWebHashHistory } from 'vue-router';
import Layout from '@/layout/index.vue';

export const constantRoutes = [
    // 首頁
    {
        path: '/',
        redirect: '/home/index',
        component: Layout,
        children: [
            {
                path: 'home/index',
                component: () => import('@/views/home/index.vue'),
                meta: { title: '首頁' },
                name: 'home'
            }
        ]
    },
    // 實驗室
    {
        path: '/lab',
        redirect: '/lab/d3',
        component: Layout,
        meta: { title: '實驗室' },
        alwaysShow: true,
        name: 'lab',
        children: [
            // d3.js
            {
                path: 'd3',
                component: () => import('@/views/lab/d3.vue'),
                meta: { title: 'd3.js' },
                name: 'd3'
            },
            // three.js
            {
                path: 'three',
                component: () => import('@/views/lab/three.vue'),
                meta: { title: 'three.js' },
                name: 'three'
            }
        ]
    },
    // 關於
    {
        path: '/about',
        redirect: '/about/index',
        component: Layout,
        children: [
            {
                path: 'index',
                component: () => import('@/views/about/index.vue'),
                meta: { title: '關於' },
                name: 'about'
            }
        ]
    },
    // 聯繫
    {
        path: '/contact',
        component: Layout,
        children: [
            {
                path: 'index',
                component: () => import('@/views/contact/index.vue'),
                meta: { title: '聯繫' }
            }
        ]
    },
    {
        path: '/404',
        component: () => import('@/views/errorPage/404.vue'),
        hidden: true
    },
    {
        path: '/:catchAll(.*)',
        redirect: '/404',
        hidden: true
    }
];

// permission router
export const asyncRoutes = [
    
];

const createCustomRouter = () => createRouter({
    // mode: 'history', // require service support
    history: createWebHashHistory(),
    scrollBehavior: () => ({ top: 0 }),
    routes: constantRoutes
});
  
const router = createCustomRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export const resetRouter = () => {
    const newRouter = createCustomRouter();
    router.resolve = newRouter.resolve; // reset router
};

export default router;
