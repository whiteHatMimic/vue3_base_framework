const state = {
    isLoading: false
};

const mutations = {
    SET_LOADING: (state, isLoading) => {
        state.isLoading = isLoading;
    }
};

const actions = {
    changeLoading: ({commit}, isLoading) => {
        commit('SET_LOADING', isLoading);
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};