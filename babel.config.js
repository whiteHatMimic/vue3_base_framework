module.exports = {
    presets: [
        '@babel/preset-env',
        '@vue/cli-plugin-babel/preset'
    ],
    plugins: [
        'dynamic-import-node'
    ]
};
